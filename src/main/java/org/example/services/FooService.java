package org.example.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FooService {
	private Logger logger = LoggerFactory.getLogger(FooService.class);
	public void bar() {
		logger.error("bar");
		logger.warn("bar");
		logger.info("bar");
		logger.debug("bar");
		logger.trace("bar");
	}
}

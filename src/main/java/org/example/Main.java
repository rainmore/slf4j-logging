package org.example;

import org.example.config.FooConfig;
import org.example.config.logging.RootLevelPropertyDefiner;
import org.example.config.logging.WorkerConfig;
import org.example.services.FooService;
import org.slf4j.event.Level;

import java.io.File;
import java.net.URL;
import java.util.Set;

public class Main {
	public static void main(String[] args) {
		System.out.println("Hello world!");

//		loadJULConfig();
//		loadLog42Config();
		loadLogbackConfig();

		FooConfig fooConfig = new FooConfig();
		fooConfig.bar();

		FooService fooService = new FooService();
		fooService.bar();
	}

	private static void loadJULConfig() {
		File logging = getLoggingProperties("jul.properties");
		if( logging.exists() && !System.getProperties().contains("java.util.logging.config.file")){
			System.setProperty("java.util.logging.config.file", logging.getAbsolutePath());
		}
		else {
			System.out.println("can't find the logging properties");
		}
	}

	private static void loadLog42Config() {
		File logging = getLoggingProperties("log4j2.properties");


		if( logging.exists() && !System.getProperties().contains("log4j2.configurationFile")){
			System.setProperty("log4j2.configurationFile", logging.getAbsolutePath());
		}
		else {
			System.out.println("can't find the logging properties");
		}
	}

	private static void loadLogbackConfig() {
		File logging = getLoggingProperties("worker/logback-worker.xml");


		if( logging.exists() && !System.getProperties().contains("logback.configurationFile")){
			System.setProperty("logback.configurationFile", logging.getAbsolutePath());
		}
		else {
			System.out.println("can't find the logging properties");
		}

		WorkerConfig workerConfig = new WorkerConfig();
		workerConfig.setConfigPath("./out/");
		workerConfig.setPort(10001);
		workerConfig.setRootLevel(Level.INFO);
		workerConfig.setAppenders(Set.of(WorkerConfig.Appenders.FILE));
		workerConfig.initProperties();

		// NOTE: <define> is triggered before application starts so the constructor can't have parameters
		RootLevelPropertyDefiner rootLevelPropertyDefiner = new RootLevelPropertyDefiner(workerConfig);
	}

	private static File getLoggingProperties(String fileName) {
		try {
			URL url = Main.class.getClassLoader().getResource(fileName);
			File loggingFile = new File(url.getFile());
			System.out.println(String.format("Logging File: %s, full path: %s", fileName, loggingFile.getAbsolutePath()));
			return loggingFile;
		} catch (NullPointerException ex) {
			return null;
		}

	}
}

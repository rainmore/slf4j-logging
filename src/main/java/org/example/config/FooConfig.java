package org.example.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FooConfig {
	private Logger logger = LoggerFactory.getLogger(FooConfig.class);


	public void bar() {
		logger.error("foo");
		logger.warn("foo");
		logger.info("foo");
		logger.debug("foo");
		logger.trace("foo");
	}
}

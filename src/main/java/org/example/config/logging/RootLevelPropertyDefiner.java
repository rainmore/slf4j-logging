package org.example.config.logging;

import ch.qos.logback.core.PropertyDefinerBase;

public class RootLevelPropertyDefiner extends PropertyDefinerBase {

	private WorkerConfig workerConfig;

	public RootLevelPropertyDefiner(WorkerConfig workerConfig) {
		this.workerConfig = workerConfig;
	}

	public String getRootLevel() {
		return workerConfig.getRootLevel().toString();
	}

	@Override
	public String getPropertyValue() {
		return getRootLevel();
	}

}

package org.example.config.logging;

import org.slf4j.event.Level;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class WorkerConfig {

	private Level rootLevel;
	private String configPath;
	private Integer port;
	private Set<Appenders> appenders;

	public enum Appenders {
		FILE,
		STDOUT
	}

	public Level getRootLevel() {
		return rootLevel;
	}

	public void setRootLevel(Level rootLevel) {
		this.rootLevel = rootLevel;
	}

	public String getConfigPath() {
		return configPath;
	}

	public void setConfigPath(String configPath) {
		this.configPath = configPath;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public Set<Appenders> getAppenders() {
		return appenders;
	}

	public void setAppenders(Set<Appenders> appenders) {
		this.appenders = appenders;
	}

	public void initProperties() {
		Optional.ofNullable(getRootLevel())
				.ifPresent(rootLevel -> System.setProperty("logback.jobServer.worker.rootLevel", rootLevel.toString()));
		Optional.ofNullable(getPort())
				.ifPresent(port -> System.setProperty("logback.jobServer.worker.port", port.toString()));
		Optional.ofNullable(getConfigPath())
				.ifPresent(path -> System.setProperty("logback.jobServer.worker.configPath", path));

		Optional.ofNullable(getAppenders())
				.filter(appenders -> !appenders.isEmpty())
				.map(appenders -> appenders.stream().map(Appenders::toString)
						.collect(Collectors.joining(",")))
				.ifPresent(string -> System.setProperty("logback.jobServer.worker.appenders", string));
	}
}
